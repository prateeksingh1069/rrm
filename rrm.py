#!/usr/bin/env python
"""
REPUTATION ROUTING Model(RRM)
Prateek Singh
"""

import binascii
import ConfigParser
import sys
import socket
import select
import struct
import time
import os
import subprocess
import netifaces as ni
import math
import random
from messages import Message
from subprocess import Popen, PIPE

version_id = '2015-01-27'

#node states
STATE_SNIFF = 0
STATE_WAIT_CONFIRM = 1
STATE_WAIT_ACK = 2
STATE_DECISION = 3
STATE_WAIT_COMMIT = 4
STATE_WAIT_DONE = 5
STATE_EXECUTE = 6
STATE_ABORT = 7

#message types
MESSAGE_CONFIRM = 'CONFIRM'
MESSAGE_ACK = 'ACK'
MESSAGE_COMMIT = 'COMMIT'
MESSAGE_DONE = 'DONE'
MESSAGE_LIST = 'LIST'

COUNT_CONFIRM = 0
COUNT_ACK = 0
COUNT_COMMIT = 0
COUNT_DONE = 0
COUNT_LIST = 0
port = 2000

def get_millis():
    return int(round(time.time() * 1000))

def write_log(message):
    log.write(message)

def get_ip_address(ifname):
    return ni.ifaddresses(ifname)[ni.AF_INET][0]['addr']

def get_broadcast_address(ifname):
    return ni.ifaddresses(ifname)[ni.AF_INET][0]['broadcast']

def send_broadcast(data):
#    if debug: 
#        print 'sending broadcast: ' + data
    sock_local.sendto(data, bcast_address)

def send_unicast(data, dest_ip):
#    if debug: 
#        print 'sending unicast to ' + dest_ip + ': ' + data
    sock_local.sendto(data, (dest_ip, port))

def send_message_unicast(message_type, dest_ip, node_list, data):
    tstamp = str(get_millis())
    message_id = 0 # TODO: implement
    msg = Message(message_type, message_id, local_ip, dest_ip, tstamp, node_list, data)
    send_unicast(str(msg), dest_ip)
    write_log(tstamp + ': sent ' + msg.getLogString() + ' to ' + dest_ip + '\n')

def send_message_broadcast(message_type, node_list, data):
    tstamp = str(get_millis())
    message_id = 0 # TODO: implement
    msg = Message(message_type, message_id, local_ip, bcast_ip, tstamp, node_list, data)
    send_broadcast(str(msg))
    write_log(tstamp + ': sent ' + msg.getLogString() + ' to ' + bcast_ip + '\n')

def send_confirm_broadcast():
    global COUNT_CONFIRM
    
    send_message_broadcast(MESSAGE_CONFIRM, acked_nodes, generate_data(size_confirm))
    COUNT_CONFIRM += 1
    
def send_confirm_unicast(dest_ip):
    global COUNT_CONFIRM
    
    send_message_unicast(MESSAGE_CONFIRM, dest_ip, acked_nodes, generate_data(size_confirm))
    COUNT_CONFIRM += 1

def send_ack_unicast(dest_ip):
    global COUNT_ACK
    
    send_message_unicast(MESSAGE_ACK, dest_ip, acked_nodes, generate_data(size_ack))
    COUNT_ACK += 1

def send_ack_broadcast():
    global COUNT_ACK
    
    send_message_broadcast(MESSAGE_ACK, acked_nodes, generate_data(size_ack))
    COUNT_ACK += 1

def send_commit_unicast(dest_ip):
    global COUNT_COMMIT
    
    send_message_unicast(MESSAGE_COMMIT, dest_ip, executed_nodes, generate_data(size_commit))
    COUNT_COMMIT += 1

def send_commit_broadcast():
    global COUNT_COMMIT
    
    send_message_broadcast(MESSAGE_COMMIT, executed_nodes, generate_data(size_commit))
    COUNT_COMMIT += 1

def send_done_unicast(dest_ip):
    global COUNT_DONE
    
    send_message_unicast(MESSAGE_DONE, dest_ip, acked, generate_data(size_done))
    COUNT_DONE += 1

def send_done_broadcast():
    global COUNT_DONE
    
    send_message_broadcast(MESSAGE_DONE, acked, generate_data(size_done))
    COUNT_DONE += 1

def send_list_unicast(dest_ip):
    global COUNT_LIST
    
    print 'INFO: LIST messages disabled'
    #send_message_unicast(MESSAGE_LIST, dest_ip, all_nodes, generate_data(size_list))
    COUNT_LIST += 1

def send_list_broadcast():
    global COUNT_LIST
    
    print 'INFO: LIST messages disabled'
    #send_message_broadcast(MESSAGE_LIST, all_nodes, generate_data(size_list))
    COUNT_LIST += 1
    
def change_state(new_state):
    global CURRENT_STATE
    global current_state_start_millis
    
    CURRENT_STATE = new_state
    current_state_start_millis = get_millis()

def generate_data(size):
    return '\x21' * size #! character

# sniff OLSR traffic to determine neighbor nodes
def sniff_neighbors():
    sniff_start_millis = get_millis()
    current_millis = sniff_start_millis
    sock_sniff = None

    try:
        sock_sniff = socket.socket( socket.AF_PACKET , socket.SOCK_RAW , socket.ntohs(0x0003))
    except socket.error, (value,message):
        if sock_sniff:
            sock_sniff.close()
    
        print 'ERROR, Could not open socket: ' + message
        sys.exit(1)
    
    while current_millis - sniff_start_millis < sniff_timeout:
        current_millis = get_millis()
    
        packet = sock_sniff.recvfrom(65565)
         
        #packet string from tuple
        packet = packet[0]
         
        #parse ethernet header
        eth_length = 14
         
        eth_header = packet[:eth_length]
        eth = struct.unpack('!6s6sH' , eth_header)
        eth_protocol = socket.ntohs(eth[2])
        
        #Parse IP packets, IP Protocol number = 8
        if eth_protocol == 8 :
            #Parse IP header
            #take first 20 characters for the ip header
            ip_header = packet[eth_length:20+eth_length]
             
            #now unpack them :)
            iph = struct.unpack('!BBHHHBBH4s4s' , ip_header)
     
            version_ihl = iph[0]
            ihl = version_ihl & 0xF
     
            iph_length = ihl * 4
    
            #ttl = iph[5]
            protocol = iph[6]
            src_ip = socket.inet_ntoa(iph[8]);
            #dst_ip = socket.inet_ntoa(iph[9]);
 
            if protocol == 17 :
                u = iph_length + eth_length
                #udph_length = 8
                udp_header = packet[u:u+8]
 
                #now unpack them :)
                udph = struct.unpack('!HHHH' , udp_header)
                source_port = udph[0]
                #dest_port = udph[1]

                #limit to OLSR packets
                if source_port == 698:
                    #print myid + ': OLSR packet from ' + str(src_ip)
                    update_neighbor(src_ip)
    sock_sniff.close()

def run_process(exe):
    p = subprocess.Popen(exe, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #p = subprocess.call(exe])
    while(True):
        retcode = p.poll() #returns None while subprocess is running
        line = p.stdout.readline()
        yield line
        if(retcode is not None):
            break

#ospf_neighbor_script = os.path.dirname(os.path.realpath(__file__)) + '/neighbors.sh'
def update_ospf_neighbors():
    ospf_neighbor_script = dir_name + '/neighbors.sh'
    for neighbor_ip in run_process(ospf_neighbor_script.split()):
        if '.' in neighbor_ip:
            update_neighbor(neighbor_ip.rstrip())
    #print myid + 'neighbor_ip: ' + neighbor_ip
    #print myid + 'neighbor_ip.rstrip(): ' + neighbor_ip.rstrip()

def run_process1(exe):
    proc = subprocess.Popen(exe, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    data = proc.stdout.readline() #block / wait
    yield data
            
def update_olsr_nextHop():
    global next_hop
    global dest
    direct_hop = '0.0.0.0'
    olsr_nextHop_script = dir_name + '/next_hop.sh'
    for ip in run_process1(olsr_nextHop_script.split()):
        next_hop = ip.rstrip()
    if not (direct_hop in next_hop):
        if not destination:
            write_log('next_hop = ' + str(next_hop) + '\n')
        else:
            write_log('next_hop = NA, Its the destination \n')
    else:
        write_log('next_hop = Destination = ' + str(dest) + '\n')
    if direct_hop in next_hop:
        next_hop = dest
    
        
def update_olsr_nextHop1(dest):
    global next_hop
    destin = dest
    Process=subprocess.Popen("route -n | awk '{if(NR>1)print $1 " " $2}' | awk '{ if ( $1 ~ /^%s$/ ) { print $2  } }'" % (str(destin),), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #data = Process.stdout.readline()
    out = Process.communicate()[0]
    write_log('Process = ' + str(Process) + '\n')
    write_log('out = ' + str(out) + '\n')
    #for ip in data.rstrip():
    next_hop = out.rstrip()
    if not (direct_hop in next_hop):
        if not destination:
            write_log('next_hop = ' + str(next_hop) + '\n')
        else:
            write_log('next_hop = NA, Its the destination \n')
    else:
        write_log('next_hop = Destination = ' + str(dest) + '\n')        

################# Function to send data to OLSR using Api.cc        
def send_pipe_info(ip, pipe_n, id, met):
    write_log('ip = ' + str(ip) + '\n')
    write_log('pipe_n = ' + str(pipe_n) + '\n')
    write_log('id = ' + str(id) + '\n')
    write_log('met = ' + str(met) + '\n')
    Process=Popen('/home/quest/Downloads/RRA/Api %s %s %s %s' % (str(ip),str(pipe_n),str(id),str(met),), shell=True)
#################
    

def update_relay_nodes():
    #relay_nodes = []
    f=open("/home/quest/Downloads/CONF/test.txt","r")
    lines=f.readlines()
    #result=[]
    for x in lines:
        relay_nodes.append(x.rstrip())
    write_log(str(sorted(relay_nodes)) + 'str(sorted(relay_nodes))\n')    

    for y in relay_nodes:
        if y in neighbor_nodes:
            ngh_relay_nodes.append(y.rstrip())
    write_log(str(sorted(ngh_relay_nodes)) + 'str(sorted(ngh_relay_nodes))\n')   

def open_sockets():
    global sock_local
    global sock_bcast
    global sockets
    
    try:
        # unicast
        sock_local = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # SOCK_DGRAM for UDP socket
        sock_local.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock_local.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock_local.bind(local_address)
        
        # broadcast
        sock_bcast = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # SOCK_DGRAM for UDP socket
        sock_bcast.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock_bcast.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock_bcast.bind(bcast_address)
        sock_bcast.setblocking(0)
        
        # combined
        sockets = [sock_local,sock_bcast]
    except socket.error, (value,message):
        if sock_local:
            sock_local.close()
        if sock_bcast:
            sock_bcast.close()
        print 'ERROR, Could not open socket(s): ' + message
        sys.exit(1)

def listen_sockets(timeout_function):
    while True:
        inputready,outputready,exceptready = select.select(sockets,[],[],select_timeout)
        # hit select_timeout and no sockets have messages
        if not (inputready or outputready or exceptready):
            if timeout_function() == 1:
                break
            else:
                continue
        # check for message on sockets
        for s in inputready:
            # unicast message
            if s == sock_local:
                data, addr = sock_local.recvfrom(buffer_size)
                #print data + ' from ' + src_ip
                #if (src_ip != local_ip):
                    #print myid + ':UNICAST FROM ' + src_ip + ': ' + data
            # broadcast message
            elif s == sock_bcast:
                data, addr = sock_bcast.recvfrom(buffer_size)
                print data + ' from ' + src_ip
                #if (src_ip != local_ip):
                    #print myid + ':BROADCAST FROM ' + src_ip + ': ' + data
            else:
                print myid + ' ERROR: received on unknown socket'
                continue
            
            # process message
            src_ip = addr[0]
            #update_neighbor(src_ip) # Newly Commented
            check_neighbor_timeouts()
            if src_ip != local_ip:
                # the packet is a valid message
                if data.startswith(MESSAGE_CONFIRM) or data.startswith(MESSAGE_COMMIT) or data.startswith(MESSAGE_ACK) or data.startswith(MESSAGE_DONE) or data.startswith(MESSAGE_LIST):
                    message = Message.fromString(data)
                    write_log(str(get_millis()) + ': received ' + message.getLogString() + ' from ' + str(src_ip) + '\n')
                    process_message(message, src_ip)
                    #print message

#update neighbor contact time
def update_neighbor(neighbor_ip):
    if neighbor_ip != local_ip:
        neighbor_nodes[neighbor_ip] = get_millis()
        if debug: 
            print myid + ': added/updated neighbor: ' + str(neighbor_ip)
    #print myid + 'neighbor_nodes: ' + neighbor_nodes
    #write_log(str(sorted(neighbor_nodes)) + 'inside update_neighbor(neighbor_ip) str(sorted(neighbor_nodes)):\n')
    #write_log(neighbor_nodes + 'inside update_neighbor(neighbor_ip) neighbor_nodes:\n')
    

def refresh_neighbors():
    #TODO clear neighbor_nodes and call update_ospf_neighbors
    print 'TODO'

#TODO: update via NHDP
def check_neighbor_timeouts():
    current_millis = get_millis()
    for neighbor in neighbor_nodes.keys():
        if current_millis - neighbor_nodes[neighbor] > neighbor_timeout:
            del neighbor_nodes[neighbor]
            if debug: 
                print myid + ': deleting neighbor due to timeout: ' + str(neighbor)

def main_timeout_conditional():
    global CURRENT_STATE
    global current_state_start_millis
    global done_retries
    global ack_retries
    global new_node
    global new_node_millis
    
    #node list update
    if new_node:
        current_millis = get_millis()
        if current_millis - new_node_millis > list_timeout:
            print myid + ': hit LIST timeout...'
            print myid + ': sending LIST broadcast'
            send_list_broadcast()
            new_node = False
    
    #sent CONFIRM, waiting for ACK
    if CURRENT_STATE == STATE_WAIT_ACK:
        current_millis = get_millis()
        if current_millis - current_state_start_millis > ack_timeout:
            ack_retries -= 1
            if debug: 
                print myid + ': hit ACK timeout...'
                print myid + ': RETRIES = ' + str(ack_retries)
            # no neighbors or all acked or timeout
            if (len(list(set(neighbor_nodes) - set(acked_nodes))) == 0) or (ack_retries == 0):
                if debug: 
                    if done_retries == 0:
                        print myid + ': out of retries, missing responses from ' + str(list(set(neighbor_nodes) - set(acked_nodes)))
                    else:
                        print myid + ': no neighbors or all acked'
                if root:
                    change_state(STATE_DECISION)
                    print myid + ': -> DECISION'
                else:
                    if debug: 
                        print myid + ': broadcasting ACK'
                    send_ack_broadcast()
                    change_state(STATE_WAIT_COMMIT)
                    print myid + ': -> WAIT_COMMIT'
            # still waiting for neighbors to ACK
            else:
                if debug: 
                    print myid + ': waiting for neighbors ' + str(list(set(neighbor_nodes) - set(acked_nodes)))
                    print myid + ': resending CONFIRM broadcast'
                send_confirm_broadcast()
            current_state_start_millis = get_millis()
    elif CURRENT_STATE == STATE_WAIT_COMMIT:
        current_millis = get_millis()
        if current_millis - current_state_start_millis > commit_timeout:
            if debug:
                print myid + ': hit COMMIT timeout...'
            change_state(STATE_ABORT)
            print myid + ': -> ABORT'
    #sent COMMIT, waiting for DONE
    elif CURRENT_STATE == STATE_WAIT_DONE:
        current_millis = get_millis()
        if current_millis - current_state_start_millis > done_timeout:
            done_retries -= 1
            if debug: 
                print myid + ': hit DONE timeout...'
                print myid + ': RETRIES = ' + str(done_retries)
            if (len(list(set(neighbor_nodes) - set(executed_nodes))) == 0) or (done_retries == 0):
                if debug: 
                    if done_retries == 0:
                        print myid + ': out of retries, missing responses from ' + str(list(set(neighbor_nodes) - set(executed_nodes)))
                    else:
                        print myid + ': no neighbors or all executed'
                if root:
                    change_state(STATE_EXECUTE)
                    print myid + ': -> EXECUTE'
                else:
                    if confirmation:
                        if debug: 
                            print myid + ': broadcasting DONE'
                        send_done_broadcast()
                    change_state(STATE_EXECUTE)
                    print myid + ': -> EXECUTE'
            else:
                if debug: 
                    print myid + ': waiting for neighbors ' + str(list(set(neighbor_nodes) - set(executed_nodes)))
                    print myid + ': resending COMMIT broadcast'
                send_commit_broadcast()
            current_state_start_millis = get_millis()
    #received ACKs, decide to EXECUTE or ABORT
    elif CURRENT_STATE == STATE_DECISION:
        current_millis = get_millis()
        if current_millis - current_state_start_millis > decision_timeout:
            print 'TODO: IMPLEMENT DECISION'
            decision = True # TODO: implement decision process
            if decision:
                send_commit_broadcast()
                if confirmation:
                    change_state(STATE_WAIT_DONE)
                    print myid + ': -> WAIT_DONE'
                else:
                    change_state(STATE_EXECUTE)
                    print myid + ': -> EXECUTE'
            else:
                change_state(STATE_ABORT)# TODO: remove this state or go back to start?
                #TODO: send ABORT message?
                #TODO: nodes reset state to WAIT_CONFIRMATION? or exit?
                #abort()
                print myid + ': ABORTING'
    #execute configuration
    elif CURRENT_STATE == STATE_EXECUTE:
        current_millis = get_millis()
        if current_millis - current_state_start_millis > executed_timeout:
            if debug: 
                print myid + ': hit execute timeout...'
            execute()
    #abort configuration
    elif CURRENT_STATE == STATE_ABORT:
        abort()
    return 0

def main_timeout_unconditional():
    global CURRENT_STATE
    global current_state_start_millis
    global done_retries
    global new_node
    global new_node_millis
    global ack_recv
    
    #node list update
    if new_node:
        current_millis = get_millis()
        if current_millis - new_node_millis > list_timeout:
            print myid + ': hit LIST timeout...'
            print myid + ': sending LIST broadcast'
            send_list_broadcast()
            new_node = False
    
    #ack_recv = 0
    #sent COMMIT, waiting for DONE
    if CURRENT_STATE == STATE_WAIT_DONE:
        current_millis = get_millis()
        if current_millis - current_state_start_millis > done_timeout:
            done_retries -= 1
            if debug: 
                print myid + ': hit DONE timeout...'
                print myid + ': RETRIES = ' + str(done_retries)
#######################################################Newly Added
            if not destination:
                #if (dest in str(acked)) or (done_retries == 0):
                if (dest in str(acked)):
                    ack_recv = ack_recv + 1                
                    if root:
                        time.sleep(2)
                        return 1 #### 1 means break, 0 means continue
                    else:
                        #send_done_unicast(previous_hop)
                        time.sleep(2)
                        return 1 #### 1 means break, 0 means continue
                else:
                    if root:
                        current_state_start_millis = get_millis() 
                        time.sleep(2)
                        execute()
                        return 1                        
                    else:
                        #send_done_unicast(previous_hop)
                        time.sleep(2)
                        execute()
                        return 1
                current_state_start_millis = get_millis()
            else:
                #send_done_unicast(previous_hop)
                ack_recv = ack_recv + 1
                time.sleep(2)
                return 1
    #abort configuration
    elif CURRENT_STATE == STATE_ABORT:
        abort()
        return 0

#process valid messages
def process_message(message, src_ip):
    global CURRENT_STATE
    global current_state_start_millis
    global start_millis
    global new_node
    global new_node_millis
    global previous_hop
    global next_hop
    global pkt_sent_other
    global ack_recv_other
    global ack_sent_by_dest
    #global destination

    #received LIST message
    if message.getType() == MESSAGE_LIST:
        print myid + ': LIST from ' + src_ip
        if not (src_ip in all_nodes):
            all_nodes.append(src_ip)
            new_node = True
            if debug:
                print myid + ': new node ' + src_ip
            new_node_millis = get_millis()
        for node_ip in message.getNodeList():
            if not (node_ip in all_nodes):
                all_nodes.append(node_ip)
                new_node = True
                if debug:
                    print myid + ': new node ' + node_ip
                new_node_millis = get_millis()
    #received CONFIRM message           
    elif message.getType() == MESSAGE_CONFIRM:
        print myid + ': CONFIRM from ' + src_ip      
        if not (src_ip in acked_nodes):
            if debug: 
                print myid + ': ACK from new node ' + src_ip
            acked_nodes.append(src_ip)
        for node_ip in message.getNodeList():
            if not (node_ip in acked_nodes):
                if debug: 
                    print myid + ': ACK from new node ' + node_ip
                acked_nodes.append(node_ip)
        #first CONFIRM
        if CURRENT_STATE == STATE_WAIT_CONFIRM:
            if not root:
                if debug: 
                    print myid + ': CONFIRM from ' + src_ip
                start_millis = get_millis()
                send_confirm_broadcast()
                if debug: 
                    print myid + ': sent CONFIRM broadcast'
                if confirmation:
                    change_state(STATE_WAIT_ACK)
                    print myid + ': -> WAIT_ACK'
                else:
                    change_state(STATE_EXECUTE)
                    print myid + ': -> WAIT_COMMIT'
            else:
                if debug: 
                    print myid + ': ROOT received CONFIRM from ' + src_ip
        else:
            if debug: 
                print myid + ': already sent CONFIRM...'
    #received ACK message
    elif message.getType() == MESSAGE_ACK:
        print myid + ': ACK from ' + src_ip
        #add/extend list of acked nodes
        if not (src_ip in acked_nodes):
            if debug: 
                print myid + ': ACK from new node ' + src_ip
            acked_nodes.append(src_ip)
        for node_ip in message.getNodeList():
            if not (node_ip in acked_nodes):
                if debug: 
                    print myid + ': ACK from new node ' + node_ip
                acked_nodes.append(node_ip)
    #received COMMIT message
    elif message.getType() == MESSAGE_COMMIT:
        print myid + ': COMMIT from ' + src_ip
        if not (src_ip in executed_nodes):
            if debug: 
                print myid + ': COMMIT from new node ' + src_ip
            executed_nodes.append(src_ip)
            
        if not (local_ip in acked):
            acked.append(local_ip) 
            
        if CURRENT_STATE == STATE_WAIT_COMMIT:
            if not root:
                previous_hop = src_ip 
                start_millis = get_millis() #
                ############Newly added
                #for neighbor in next_hop:
                if not str(myid) == 'n18':
                    if not destination:
                            write_log(str(get_millis()) + ' Routing started by other node for Packet no(pkt_sent_other): ' + str(pkt_sent_other+1) + '\n')                            
                            send_commit_unicast(next_hop)
                            pkt_sent_other = pkt_sent_other + 1
                            previous_hop = src_ip
                            send_done_unicast(previous_hop)
                    if destination:
                        previous_hop = src_ip
                        send_done_unicast(previous_hop)
                        ack_sent_by_dest = ack_sent_by_dest + 1
                        #next_hop  = src_ip 
                else:
                    k = random.random()
                    if (k > 0.4): # for loss percentagel
                        write_log(str(get_millis()) + ' Routing started by malicious node for Packet no(pkt_sent_other): ' + str(pkt_sent_other+1) + '\n')
                        send_commit_unicast(next_hop)
                        pkt_sent_other = pkt_sent_other + 1
                        previous_hop = src_ip
                        send_done_unicast(previous_hop)                        
                ############    
                if debug: 
                    print myid + ': sending COMMIT broadcast'
                    change_state(STATE_WAIT_DONE)
                    print myid + ': -> WAIT_DONE'
        else:
            if debug: 
                print myid + ': already sent COMMIT...'
    #received DONE message
    elif message.getType() == MESSAGE_DONE:
##################################### Newly added code
        print myid + ': DONE from ' + src_ip
        #add/extend list of executed nodes
        if not (src_ip in done_executed_nodes):
            if debug: 
                print myid + ': DONE from new node ' + src_ip
            done_executed_nodes.append(src_ip)
        done_n = message.getNodeList()
        
        for z in done_executed_nodes:
            if not (z in acked):
                acked.append(z.rstrip())
                
        for x in done_n:
            if not (x in done_nodes):
                done_nodes.append(x.rstrip())
                
        for y in done_nodes:
            if not (y in acked):
                acked.append(y.rstrip())
                
        ack_recv_other = ack_recv_other + 1        
        #write_log(str(sorted(acked)) + 'str(sorted(acked))\n')
#####################################
    else:
        print myid + ': ERROR unknown message type ' + message.getType() + ' from ' + src_ip

def execute():
    global log
    
    stop_millis = get_millis()
    print myid + '(' + local_ip + ') executed in ' + str(stop_millis - start_millis) + ' msec'
    
    if debug: 
        print myid + ' all node list: ' + str(sorted(all_nodes))
        print myid + ' neighbor list: ' + str(sorted(neighbor_nodes))
        print myid + ' acked list: ' + str(sorted(acked_nodes))
        print myid + ' executed list: ' + str(sorted(executed_nodes))
        print myid + ' counts: CONFIRM=' + str(COUNT_CONFIRM) + ', ACK=' + str(COUNT_ACK) + ', COMMIT=' + str(COUNT_COMMIT) + ', DONE=' + str(COUNT_DONE) + ', LIST=' + str(COUNT_LIST)
    return 0

def abort():
    global log
    
    print myid + '(' + local_ip + ') aborted'
    
    if debug: 
        print myid + ' all node list: ' + str(sorted(all_nodes))
        print myid + ' neighbor list: ' + str(sorted(neighbor_nodes))
        print myid + ' acked list: ' + str(sorted(acked_nodes))
    
    write_log(str(get_millis()) + ': rollout aborted\n')
    log.close()
    sock_local.close()
    log = open(abort_file, 'w')
    log.write(myid)
    log.close()
    exit(1)

def TCM_Calculation():
    global next_hop
    global direct_hop
    global dest 
    global ack_sent_by_dest    
    ###################Calculating trust metric using method 1
    # write_log(' ack_pck: ######################## : ' + str(ack_recv) + '\n')
    # write_log(' pkt_sent: ######################## : ' + str(pkt_sent) + '\n')
    # p_e = (1 - (float(ack_recv)/pkt_sent))
    # write_log(' p_e: ######################## : ' + str(p_e) + '\n')
    # if p_e == 0:
        # p_e = 0.1
    # log_p_e = math.log10(p_e)
    # write_log(' log_p_e: ######################## : ' + str(log_p_e) + '\n')
    # metr = (-1)*(255)*(log_p_e)
    ###################  

    ###################Calculating trust metric using method 2
    # alpha = 0.8
    # p_e = (float(ack_recv)/pkt_sent)
    # if p_e == 0:
        # p_e = 0.1
    # log_p_e = math.log10(p_e)
    # metr = (-1)*(255)*(log_p_e)
    # avg_link_metr = (alpha*previous_mean_val + (1-alpha)*metr)
    # write_log(' avg_link_metr: ######################## : ' + str(avg_link_metr) + '\n')
    ###################  

    ###################Calculating trust metric using method 3
    if root:
        write_log(' ack_recv_other: ######################## : ' + str(ack_recv_other) + '\n')
        write_log(' pkt_sent: ######################## : ' + str(pkt_sent) + '\n')
        p_e = (1 - (float(ack_recv_other)/pkt_sent))
        write_log(' p_e: ######################## : ' + str(p_e) + '\n')
        if p_e == 0:
            p_e = 0.01
        #log_p_e = math.log10(p_e)
        #write_log(' log_p_e: ######################## : ' + str(log_p_e) + '\n')
        metr = (255)*(p_e)
    elif destination:
        write_log(' ack sent by dest: ######################## : ' + str(ack_sent_by_dest) + '\n')
        write_log(' pkt_sent_total: ######################## : ' + str(pkt_sent_total) + '\n')
        p_e = (1 - (float(ack_sent_by_dest)/pkt_sent_total))
        write_log(' p_e: ######################## : ' + str(p_e) + '\n')
        if p_e == 0:
            p_e = 0.01
        #log_p_e = math.log10(p_e)
        #write_log(' log_p_e: ######################## : ' + str(log_p_e) + '\n')
        metr = (255)*(p_e)    
    else:
        write_log(' ack_recv_other: ######################## : ' + str(ack_recv_other) + '\n')
        write_log(' pkt_sent_other: ######################## : ' + str(pkt_sent_other) + '\n')
        p_e = (1 - (float(ack_recv_other)/pkt_sent_other))
        write_log(' p_e: ######################## : ' + str(p_e) + '\n')
        if p_e == 0:
            p_e = 0.01
        #log_p_e = math.log10(p_e)
        #write_log(' log_p_e: ######################## : ' + str(log_p_e) + '\n')
        metr = (255)*(p_e)    
    ###################     
    
    ################### Sending data to NRLOLSR using API for method 1
    if not destination:
        pipe_name = '-ebmClient'
        pipeN = myid + '_olsr'
        metric = str(metr)
        if direct_hop in next_hop:
            next_hop = dest
        #send_pipe_info(next_hop, pipe_name, pipeN, metric)
        if (pkt_sent > 3) or (pkt_sent_other > 3):
            send_pipe_info(next_hop, pipe_name, pipeN, metric)
    else:
        next_hop = mal_node
        pipe_name = '-ebmClient'
        pipeN = myid + '_olsr'
        metric = str(metr)
        #if (pkt_sent > 3) or (pkt_sent_other > 3):
        send_pipe_info(next_hop, pipe_name, pipeN, metric)        
    ###################
    
    ################### Sending data to NRLOLSR using API for method 2
    # pipe_name = '-ebmClient'
    # pipeN = myid + '_olsr'
    # metric = str(avg_link_metr) 
    # send_pipe_info(next_hop, pipe_name, pipeN, metric)    
    ################### 

def TCM_Calculation1():
    global next_hop
    global direct_hop
    global dest    
    ###################Calculating trust metric
    metr = 235
    ###################    
    
    ################### Sending data to NRLOLSR using API
    pipe_name = '-ebmClient'
    pipeN = myid + '_olsr'
    metric = str(metr)
    #send_pipe_info('10.0.0.18', pipe_name, pipeN, metric)
    #send_pipe_info('10.0.0.2', pipe_name, pipeN, metric)    
	
	
# MAIN ENTRY POINT
dir_name = os.path.dirname(os.path.abspath(__file__))


if (len(sys.argv) < 2):
    print("Usage: rra.py <node ID (n10 for root)> <config file name(config1.cfg)>")
    exit(0)

# setting the config file path name
config_dir = dir_name + '/config/' + 'config1.cfg'

# set parameters, setting node ids first
myid = sys.argv[1]

#load configuration file
if len(sys.argv) > 2:
    if os.path.isfile(config_dir):
        config_file = config_dir
    else:
        print 'ERROR: invalid config path, ' + config_dir
        sys.exit()
config = ConfigParser.RawConfigParser()
config.read(config_file)

# Defining source and destination paramemters
root = False
destination = False
malicious_node = False

######################### Experiment: 1

if str(myid) == 'n10':
    root = True
if str(myid) == 'n6':
    destination = True
if str(myid) == 'n18':
    malicious_node = True    
dest = '10.0.0.6'
source = '10.0.0.10'
mal_node = '10.0.0.18'

##########################

######################### Experiment: 2
# if str(myid) == 'n8' or str(myid) == 'n6' or str(myid) == 'n3' or str(myid) == 'n1':
    # root = True
# if str(myid) == 'n10' or str(myid) == 'n4' or str(myid) == 'n5' or str(myid) == 'n7':
    # destination = True
    
# if str(myid) == 'n8':
    # dest = '10.0.0.7'
    # source = '10.0.0.8' 

# if str(myid) == 'n3':
    # dest = '10.0.0.5'
    # source = '10.0.0.3'

# if str(myid) == 'n1':
    # dest = '10.0.0.4'
    # source = '10.0.0.1'

# if str(myid) == 'n6':
    # dest = '10.0.0.10'
    # source = '10.0.0.6'    

##########################

# Getting network information	
iface = config.get('General', 'iface')#'eth0'#emane0#lo
local_ip = get_ip_address(iface)
local_ip_test = "'" + local_ip + "'"
bcast_ip = get_broadcast_address(iface)
port = config.getint('General', 'port')#2000
local_address = (local_ip, port)
bcast_address = (bcast_ip, port)
buffer_size = 1024
debug = config.get('General', 'debug')

# Setting Log generation parameters
log_dir = dir_name + '/logs/'
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
log_file = log_dir + 'node_' + myid + '.log'
exec_file = log_dir + 'exec_' + myid + '.log'
abort_file = log_dir + 'abort_' + myid + '.log'
log = open(log_file, 'w')

#timeouts settings
sniff_timeout = config.getint('Timeout', 't_sniff')#1000
pending_timeout = config.getint('Timeout', 't_pending')#1000#500
ack_timeout = config.getint('Timeout', 't_ack')#pending_timeout
done_timeout = config.getint('Timeout', 't_done')#pending_timeout
decision_timeout = config.getint('Timeout', 't_decision')#pending_timeout
commit_timeout = config.getint('Timeout', 't_commit')#20000
neighbor_timeout = config.getint('Timeout', 't_neighbor')#40000
select_timeout = config.getint('Timeout', 't_select')#0#1
executed_timeout = config.getint('Timeout', 't_executed')#1000#500
list_timeout = config.getint('Timeout', 't_list')#5000#2000

#retries
done_retries = config.getint('Retry', 'r_done')#retries
ack_retries = config.getint('Retry', 'r_ack')#retries
retries = 220

#sizes
size_confirm = config.getint('Size', 's_confirm')##SIZE_CONFIRM = 10000
size_ack = config.getint('Size', 's_ack')#SIZE_ACK = 50
size_commit = config.getint('Size', 's_commit')#SIZE_COMMIT = 20000
size_done = config.getint('Size', 's_done')#SIZE_DONE = 10
size_list = config.getint('Size', 's_list')#SIZE_LIST = 0

if debug:  
    print 'using configuration: ' + config_file
    print '=============================================='

if debug: 
    print 'ID: ' + myid + ('(ROOT)' if root else '')
    print 'Interface: ' + iface
    print 'Local IP: ' + local_ip
    print 'Bcast IP: ' + bcast_ip
    print 'Port: ' + str(port)
    print 'Retries: ack ' + str(ack_retries) + ', done ' + str(done_retries)
    print 'Timeouts: sniff ' + str(sniff_timeout) + ', neighbor ' + str(neighbor_timeout) + ', pending/ack/done/decision ' + str(pending_timeout) + ', commit ' + str(commit_timeout) + ', executed ' + str(executed_timeout)
    print 'Sizes: confirm ' + str(size_confirm) + ', ack ' + str(size_ack) + ', commit ' + str(size_commit) + ', done ' + str(size_done) + ', list ' + str(size_list)

if root:
    write_log('log: node=' + myid + ' ip=' + str(local_ip) + ' (ROOT NODE)\n')
else:
    write_log('log: node=' + myid + ' ip=' + str(local_ip) + '\n')

#sockets
sock_local = None
sock_bcast = None
sockets = []

#node lists
neighbor_nodes = {}
all_nodes = []
all_nodes.append(local_ip)
acked_nodes = []
executed_nodes = []
new_node = False
new_node_millis = get_millis()
updating_neighbour = True # Newly Added
next_hop = None
direct_hop = '0.0.0.0'
previous_hop = None
pipe_name = '-ebmClient'
metric = '1'
pkt_sent = 1
pkt_sent_other = 1
pkt_sent_total = 0
ack_sent_by_dest = 0
ack_recv = 0 
ack_recv_other = 1
previous_mean_val = 0

#####################################################
# setup sockets
open_sockets()

##################################################### Getting Neighbor Information
CURRENT_STATE = STATE_SNIFF
if updating_neighbour:
    if debug:
        print '===================================='
        print 'querying OSPF neighbors...'
    update_ospf_neighbors()
    update_olsr_nextHop()
    #update_olsr_nextHop1(dest)
##################################################### 

# main loop - Sending packets and recieving acknowledgements
if debug: 
    print '===================================='
    print 'entering main loop... Starting Routing Process'
   
for i in range(100):
    done_executed_nodes = [] # Newly Added
    done_nodes = [] # Newly Added
    acked = [] # Newly Added 
    pkt_sent_total = i + 1
    update_olsr_nextHop()
    #update_olsr_nextHop1(dest)
    #write_log(str(get_millis()) + ' Routing started for Packet no: ' + str(pkt_sent) + '\n')    
    if root or destination:
            print myid + '(ROOT) waiting 3 seconds...'
            print myid + '(ROOT) start sending Packets'
            if destination:
                change_state(STATE_WAIT_COMMIT)
                pkt_sent_total = pkt_sent_total + 1
            #send_commit_broadcast()
            print 'next_hop: ' + str(next_hop)
            #for x in next_hop:
            if debug and root:
                x = next_hop
                if not (direct_hop in next_hop):
                    send_commit_unicast(x)
                    pkt_sent = pkt_sent + 1
                    write_log(str(get_millis()) + ' Routing started by root node for Packet no(pkt_sent): ' + str(pkt_sent) + '\n')
                else:
                    send_commit_unicast(dest)
                    pkt_sent = pkt_sent + 1
                    write_log(str(get_millis()) + ' Routing started by root node for Packet no(pkt_sent): ' + str(pkt_sent) + '\n')
                print 'neighbor: ' + x
                #print 'next_hop: ' + next_hop
            start_millis = get_millis()
            if debug and root:
                change_state(STATE_WAIT_DONE)
                print myid + ': -> WAIT_DONE'
                current_state_start_millis = get_millis() # Newly Added          
    else:
            #pkt_sent_total = i + 1
            change_state(STATE_WAIT_COMMIT)
            print myid + ': -> WAIT_COMMIT'
            current_state_start_millis = get_millis() # Newly Added
    listen_sockets(main_timeout_unconditional)
    TCM_Calculation()
    #TCM_Calculation1()

#TCM_Calculation()
if root:
    write_log(str(get_millis()) + ' Routing Process Complete: ######################## : Routing to destination ' + str(dest) + ' is Complete\n')
elif destination:
    write_log(str(get_millis()) + ' Routing Process Complete: ######################## : Recieved Packet originated from source ' + str(source) + ' and Sent Acknowledgment back....\n')
else:
    write_log(str(get_millis()) + ' Routing Process Complete: ######################## : Forwarded packet originated from source ' + str(source) + ' and Sent Acknowledgment back originated from destination ' + str(dest) + '\n')    
    
    
log.close()
sock_local.close()
sock_bcast.close()
log = open(exec_file, 'w')
log.write(myid)
log.close()
exit(0)    