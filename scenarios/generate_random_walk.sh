#!/bin/bash

#parameters from command line
if [ -z "$1" ]; then
	echo usage: $0 'name nodes'
	exit
else
	SCENARIO=$1
fi

if [ -z "$2" ]; then
	echo usage: $0 'name nodes'
	exit
else
	NODES=$2
fi

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
#BONNMOTION=../../bonnmotion-2.1.2/bin
BONNMOTION=../setup/bonnmotion-2.1.2/bin

#cd $BONNMOTION && ./bm -f $SCENARIO RandomWalk -n $NODES -d 600 -i 600 -t 100 -x 800 -y 600 -h 3 -l 1
cd $BONNMOTION && ./bm -f $SCENARIO RandomWalk -n $NODES -d 2500 -i 600 -t 19000 -x 500 -y 500 -h 60 -l 60
./bm NSFile -f $SCENARIO
cp $SCENARIO.ns_movements $DIR/
