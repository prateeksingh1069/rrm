#!/bin/bash

echo 'SETTING UP TESTING ENVIRONMENT'
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# DIR = /home/quest

#BASIC DEPENDENCIES
sudo apt-get -y install build-essential
sudo apt-get -y install python

#EMANE
#https://github.com/adjacentlink/emane/wiki
echo 'installing EMANE'
sudo apt-get -y install libxml2 libprotobuf8 python-protobuf libpcap0.8 libpcre3 libuuid1 libace-6.0.3 python-lxml python-setuptools
cd $DIR/setup/emane-0.9.2-release-1/debs/ubuntu-14_04/amd64
sudo dpkg -i emane*.deb

#EMANE TUTORIAL
#https://github.com/adjacentlink/emane-tutorial/wiki
echo 'installing EMANE demos'
sudo apt-get -y install lxc bridge-utils mgen fping gpsd gpsd-clients iperf multitail olsrd openssh-server python-tk python-pmw python-lxml
sudo apt-get -y install python-stdeb
cd $DIR/setup/pynodestatviz
make deb
sudo dpkg -i deb_dist/pynodestatviz*.deb
cd $DIR/demos
make
sudo mkdir -p /var/lib/emanenode

#HOSTS
echo 'updating hosts file'
sudo -- sh -c "echo '10.99.0.1 node-1' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.2 node-2' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.3 node-3' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.4 node-4' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.5 node-5' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.6 node-6' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.7 node-7' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.8 node-8' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.9 node-9' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.10 node-10' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.100 node-server' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.1 radio-1' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.2 radio-2' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.3 radio-3' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.4 radio-4' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.5 radio-5' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.6 radio-6' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.7 radio-7' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.8 radio-8' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.9 radio-9' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.10 radio-10' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.1 n1' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.2 n2' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.3 n3' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.4 n4' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.5 n5' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.6 n6' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.7 n7' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.8 n8' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.9 n9' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.10 n10' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.11 n11' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.12 n12' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.13 n13' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.14 n14' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.15 n15' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.16 n16' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.17 n17' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.18 n18' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.19 n19' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.20 n20' >> /etc/hosts"

#SSH CONFIG (ignore host key changes for CORE)
mkdir ~/.ssh
sudo -- sh -c "echo $'HOST n1\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n2\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n3\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n4\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n5\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n6\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n7\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n8\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n9\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n10\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n11\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n12\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n13\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n14\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n15\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n16\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n17\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n18\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n19\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n20\n\tStrictHostKeyChecking no' >> ~/.ssh/config"

#CONF
echo 'setting up CONF'
sudo apt-get -y install terminator
sudo apt-get -y install python-dev
sudo easy_install netifaces

#R
echo 'installing R'
sudo apt-get -y install r-base r-base-dev
echo 'installing ggplot2'
sudo R --file=$DIR/Downloads/CONF/setup/ggplot.R

#PERMISSIONS
echo 'setting permissions'
sudo $DIR/Downloads/CONF/sudoers.sh $USER

#CORE
#https://code.google.com/p/coreemu/wiki/Quickstart
echo 'installing CORE'
sudo apt-get -y install bash bridge-utils ebtables iproute libtk-img tcl8.5 tk8.5 autoconf automake gcc libev-dev make pkg-config libreadline-dev imagemagick help2man mgen traceroute
sudo apt-get -y install quagga
cd $DIR/setup/core && ./bootstrap.sh && ./configure && make -j8 && sudo make install
#sudo core-daemon -v
#core-gui

#Java
#http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html
echo 'installing Java'
sudo apt-add-repository -y ppa:webupd8team/java
sudo apt-get update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo apt-get -y install oracle-java8-installer
sudo apt-get -y install oracle-java8-set-default

#BonnMotion
#http://sys.cs.uos.de/bonnmotion/faq.shtml
echo 'building BonnMotion'
cd $DIR/setup/bonnmotion-2.1.2 && ./install
#cd $DIR/setup/bonnmotion-2.1.2/bin && ./bm

echo 'FINISHED'

#SSH KEY
echo 'NOTE: to finish setup perform the following steps'
echo '(or add the appropriate lines above to SSH CONFIG):'
echo 'ssh-keygen if no key exists'
echo 'with terminal in demos/zconf, sudo ./demo-start'
echo 'ssh-copy-id node-1, yes, enter password'
echo 'for nodes 2-10, ssh node-*, yes, exit'
echo 'with terminal in demos/zconf, sudo ./demo-stop'
