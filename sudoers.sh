#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
while [[ -n $1 ]]; do
    echo "adding permissions for $1"
    echo "#CONF permissions for $1" >> /etc/sudoers;
    echo "$1	ALL=(root) NOPASSWD:	$DIR/Downloads/CONF/demos/scripts/democtl-host" >> /etc/sudoers;
    echo "$1	ALL=(root) NOPASSWD:	$DIR/Downloads/CONF/demos/scripts/demo-init" >> /etc/sudoers;
    echo "$1	ALL=(root) NOPASSWD:	$DIR/Downloads/CONF/conf.py" >> /etc/sudoers;
    echo "$1	ALL=(root) NOPASSWD:	$DIR/Downloads/CONF/conf_conditional.py" >> /etc/sudoers;
    echo "$1	ALL=(root) NOPASSWD:	$DIR/Downloads/CONF/conf_unconditional.py" >> /etc/sudoers;
    echo "$1	ALL=(root) NOPASSWD:	/bin/kill" >> /etc/sudoers;
    shift # shift all parameters;
done
